﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Practice_04_12_2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void DownloadButtonClick(object sender, RoutedEventArgs e)
        {           
            Thread downloadThread = new Thread(DownloadProcess);
            downloadThread.Start(urlBox.Text);
        }
        private void DownloadProcess(object arg)
        {
            string url = arg as string;
            string[] fileName = url.Split(new char[] { '/' });

            using (var client = new WebClient())
            {
                try
                {
                    this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                   (ThreadStart)delegate ()
                   {
                       progressBar.IsIndeterminate = true;
                       downloadButton.IsEnabled = false;
                   });
                    

                    client.DownloadFile(url, fileName[fileName.Length - 1]);
                    MessageBox.Show("Скачивание завершено!");

                    this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                        (ThreadStart)delegate ()
                        {
                            progressBar.IsIndeterminate = false;
                            downloadButton.IsEnabled = true;
                        });
                }
                catch (ArgumentNullException exception) { MessageBox.Show($"Ошибка!\n{exception.Message}"); }
                catch (WebException exception) { MessageBox.Show($"Ошибка!\n{exception.Message}"); }
                catch (NotSupportedException exception) { MessageBox.Show($"Ошибка!\n{exception.Message}"); }               
            }
        }
    }
}
